Categories:Money
License:GPL-3.0
Web Site:http://igisw-bilancio.sourceforge.net/
Source Code:https://launchpad.net/bilancio
Issue Tracker:https://bugs.launchpad.net/bilancio

Auto Name:OpenMoneyBox
Summary:Budget management application
Description:
OpenMoneyBox is an application designed to manage small personal money budgets
in the easiest way.
.

Repo Type:bzr
Repo:lp:bilancio/trunk

Build:3.1.2.5,17
    commit=4
    subdir=android/API24
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.1.2.5
Current Version Code:17
